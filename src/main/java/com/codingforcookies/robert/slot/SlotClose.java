package com.codingforcookies.robert.slot;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.codingforcookies.robert.core.GUI;

public class SlotClose implements ISlotAction {
	public void doAction(GUI gui, Player p, InventoryClickEvent event) { gui.close(p); }
}